package com.kav.flight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightApplication_Main {
    public static void main(String[] args){
        SpringApplication.run(FlightApplication_Main.class,args);
    }

}
