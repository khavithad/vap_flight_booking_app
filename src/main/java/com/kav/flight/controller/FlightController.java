package com.kav.flight.controller;

import com.kav.flight.model.TravelClass;
import com.kav.flight.model.Trip;
import com.kav.flight.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@EnableAutoConfiguration
@Controller
public class FlightController {

    public FlightController(){

    }
    @Autowired
    private FlightService flightService;

    @GetMapping("/search")
    public ModelAndView search() {

        return new ModelAndView("Search.html");
    }

    @GetMapping("/allflights")
    public ModelAndView findAllFlights(@RequestParam(required = false) String origin,
                                       @RequestParam(required = false) String destination, @RequestParam(required = false)
                                               String seats, @RequestParam(required = false) String date,
                                            @RequestParam(required = false) String travelClass) {

        DateTimeFormatter df=  DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate lDate=LocalDate.parse(date,df);
        TravelClass tClassValue = convertTravelClass(travelClass);
        List<Trip> flightsList= new ArrayList();
        if (origin != null && !(origin.isEmpty()) && destination!=null && seats!=null && tClassValue!=null)
            flightsList= flightService.findFlightsWithTravelClass(origin,destination,lDate,tClassValue, new Integer(seats));
        else if (origin != null && !(origin.isEmpty()) && destination!=null && lDate!=null) {
            flightsList = flightService.findFlightsWithOriginDestination(origin, destination, lDate);
        }else
            flightsList = flightService.findAllFlights();

        ModelAndView mv= new ModelAndView("FlightResults.html");

        mv.addObject("flights",flightsList);
       return mv;
    }

    private TravelClass convertTravelClass(String travelClass) {
        if (travelClass!=null) {
            if (travelClass.equals("Business"))
                return TravelClass.BUSINESS;
            else if (travelClass.equals("First"))
                return TravelClass.FIRST;
        }
        return TravelClass.ECONOMY;
    }


    @GetMapping("/react-allflights")

    @ResponseBody
    public List<Trip> findAllFlightsForReact(@RequestParam(required = false) String origin,
                                     @RequestParam(required = false) String destination, @RequestParam(required = false)
                                               String seats, @RequestParam(required = false) String date,
                                     @RequestParam(required = false) String travelClass) {

        DateTimeFormatter df=  DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate lDate=LocalDate.parse(date,df);
        TravelClass tClassValue = convertTravelClass(travelClass);
        List<Trip> flightsList= new ArrayList();
        if (origin != null && !(origin.isEmpty()) && destination!=null && seats!=null && tClassValue!=null)
            flightsList= flightService.findFlightsWithTravelClass(origin,destination,lDate,tClassValue, new Integer(seats));
        else if (origin != null && !(origin.isEmpty()) && destination!=null && lDate!=null) {
            flightsList = flightService.findFlightsWithOriginDestination(origin, destination, lDate);
        }else
            flightsList = flightService.findAllFlights();


        return flightsList;
    }

}

