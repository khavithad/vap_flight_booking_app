package com.kav.flight.model;

import org.springframework.stereotype.Component;

@Component
public class Price {
    private int baseEconomyPrice = 6000;
    private int baseFirstPrice = 10000;
    private int baseBusinessPrice=20000;

    public Price(){

    }

    public int getBaseFirstPrice() {
        return baseFirstPrice;
    }

    public void setBaseFirstPrice(int baseFirstPrice) {
        this.baseFirstPrice = baseFirstPrice;
    }

    public int getBaseBusinessPrice() {
        return baseBusinessPrice;
    }

    public void setBaseBusinessPrice(int baseBusinessPrice) {
        this.baseBusinessPrice = baseBusinessPrice;
    }

    public int getBaseEconomyPrice() {
        return baseEconomyPrice;
    }

    public void setBaseEconomyPrice(int baseEconomyPrice) {
        this.baseEconomyPrice = baseEconomyPrice;
    }
}
