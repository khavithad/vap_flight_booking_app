package com.kav.flight.model;

import org.springframework.stereotype.Component;

@Component
public class BookedSeats {

    private int bookedBusinessSeats;
    private int bookedFirstSeats;
    private int bookedEconomySeats;

    public BookedSeats(){

    }

    public int getBookedBusinessSeats() {
        return bookedBusinessSeats;
    }

    public void setBookedBusinessSeats(int bookedBusinessSeats) {
        this.bookedBusinessSeats = bookedBusinessSeats;
    }

    public int getBookedFirstSeats() {
        return bookedFirstSeats;
    }

    public void setBookedFirstSeats(int bookedFirstSeats) {
        this.bookedFirstSeats = bookedFirstSeats;
    }

    public int getBookedEconomySeats() {
        return bookedEconomySeats;
    }

    public void setBookedEconomySeats(int bookedEconomySeats) {
        this.bookedEconomySeats = bookedEconomySeats;
    }

}
