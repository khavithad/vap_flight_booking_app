package com.kav.flight.model;

public enum TravelClass {
    BUSINESS,FIRST,ECONOMY
}
