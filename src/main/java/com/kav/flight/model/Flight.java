package com.kav.flight.model;

import org.springframework.stereotype.Component;

@Component
public class Flight {
    private String name;
    private String no;
    private Seats seats;

    public Flight(){

   }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Flight(String name, String no) {
        this.name = name;
        this.no = no;
    }


    public Flight(String name, String no, Seats seats) {
        this.name = name;
        this.no = no;
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "name='" + name + '\'' +
                ", no='" + no + '\'' +
                ", seats=" + seats +
                '}';
    }


    public Seats getSeats() {
        return seats;
    }

    public void setSeats(Seats seats) {
        this.seats = seats;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
