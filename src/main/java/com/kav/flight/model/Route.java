package com.kav.flight.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Objects;

@Component
public class Route {
    private String origin;
    private String destination;
    private LocalDate departureDate;

    @Autowired
    private Price price;
    @Autowired
    private Flight flight;

    public Route(){
    }

    public Route(String origin, String destination, LocalDate departureDate, Flight flight,Price price) {
        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.flight = flight;
        this.price = price;
    }
    public Route(String origin, String destination, LocalDate departureDate) {
        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
    }

    public Route(String origin, String destination, LocalDate departureDate, Price price) {
        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.price = price;
    }


    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    @Override
    public String toString() {
        return "Route{" +
                "origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", departureDate=" + departureDate +
                ", flight="+getFlight().getName()+
                ", price=" + price.getBaseEconomyPrice() + '}';
    }



    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Route)) return false;
        Route route = (Route) o;
        return Objects.equals(getOrigin(), route.getOrigin()) &&
                Objects.equals(getDestination(), route.getDestination()) &&
                Objects.equals(getDepartureDate(), route.getDepartureDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrigin(), getDestination(), getDepartureDate());
    }

}
