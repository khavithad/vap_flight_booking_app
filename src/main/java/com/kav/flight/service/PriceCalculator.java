package com.kav.flight.service;

import com.kav.flight.model.Route;

import java.time.LocalDate;

public interface PriceCalculator{

    void basePrice(double price);
    double calculatePrice(Route route, LocalDate date, int seatsToBeBooked);
}
