package com.kav.flight.service;

import com.kav.flight.model.Route;

import java.time.LocalDate;

public class FirstClassPriceCalculatorImpl implements PriceCalculator {
   private double price=1;
    @Override
    public void basePrice(double price) {
        this.price = price;
    }

    @Override
    public double calculatePrice(Route route, LocalDate bookingDate, int seatsToBeBooked) {
        basePrice(route.getPrice().getBaseFirstPrice());
        LocalDate todayDate= LocalDate.now();
        int diffInDays = bookingDate.compareTo(todayDate);

        if(diffInDays <= 10){
            double percent= (double) diffInDays * 10/100;
            price = price + (price * percent);
        } else {
            return 0;
        }
        return price * seatsToBeBooked;
    }
}
