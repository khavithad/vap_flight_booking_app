package com.kav.flight.service;

import java.util.Calendar;
import java.util.List;


public interface Flight_Accomadation<Flight> {

    List<Flight> findAllFlights();

    List<Flight> findFlightsWithOriginDestination(String origin, String destination, Calendar date);

    List<Flight> findFlightsWithSeats(int seats);

}
