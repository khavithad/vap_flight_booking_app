package com.kav.flight.service;

import com.kav.flight.model.Route;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class BusinessClassPriceCalculatorImpl implements PriceCalculator {
    private double price;
    @Override
    public void basePrice(double price) {
        this.price = price;
    }

    @Override
    public double calculatePrice(Route route, LocalDate date, int seatsToBeBooked) {
        Enum<DayOfWeek>  day=  date.getDayOfWeek();
        basePrice(route.getPrice().getBaseBusinessPrice());
        if(day.equals(DayOfWeek.MONDAY)|| day.equals(DayOfWeek.FRIDAY) ||day.equals(DayOfWeek.SUNDAY))
                price = price + (price * 60/100);

        return price* seatsToBeBooked;
    }
}
