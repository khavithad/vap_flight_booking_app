package com.kav.flight.service;

import com.kav.flight.dao.FlightDao;
import com.kav.flight.model.Route;
import com.kav.flight.model.TravelClass;
import com.kav.flight.model.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class FlightService {

    @Autowired
    FlightDao flightDao;

    public List<Trip> findAllFlights() {
        return flightDao.findAllFlights();
    }

    public List<Trip> findFlightsWithOriginDestination(String origin, String destination, LocalDate date) {
        return flightDao.findFlightsWithOriginDestination(origin, destination, date);
    }

    public List<Route> findFlightsWithSeats(int seats) {
        return flightDao.findFlightsWithSeats(seats);
    }

    public List<Trip> findFlightsWithTravelClass(String origin, String destination, LocalDate date, TravelClass travelClass,int seats) {

        return flightDao.findFlightsWithTravelClass(origin,destination,date,travelClass,seats);
    }
}
