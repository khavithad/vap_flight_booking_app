package com.kav.flight.service;

import com.kav.flight.model.Route;
import com.kav.flight.model.Seats;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class EconomyClassPriceCalculatorImpl implements PriceCalculator {
    private double price;
    @Override
    public void basePrice(double price) {
        this.price=price;
    }

    @Override
    public double calculatePrice(Route route, LocalDate date, int bookingSeats) {
        Seats seats = route.getFlight().getSeats();
        int totalSeats = seats.getEconomy();
        int seatsAvailable = seats.getEconomy() - seats.getBookedEconomySeats();
        int firstSeats = totalSeats*40/100;
        int nextSeats = totalSeats*50/100;
        int lastSeats = totalSeats*10/100;
        basePrice(route.getPrice().getBaseEconomyPrice());

        if(seatsAvailable <= lastSeats)
          price = (price + (price * 60/100));
        else if (seatsAvailable > lastSeats && seatsAvailable <= firstSeats)
            price =( price + (price * 30/100));

        return price * bookingSeats;
    }
}
