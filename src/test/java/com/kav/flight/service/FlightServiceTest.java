package com.kav.flight.service;

import com.kav.flight.dao.FlightDao;
import com.kav.flight.model.Route;
import com.kav.flight.model.TravelClass;
import com.kav.flight.model.Trip;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class FlightServiceTest {

   @Mock
   FlightDao dao = new FlightDao();

   List<Route> listOfFlights;
   LocalDate departureDate;
   FlightServiceTest test;

    @Before
    public void setUp() throws Exception {
        test  = new FlightServiceTest();
        test.fillFlightDetails();
        departureDate = LocalDate.now().plusDays(5);

    }

    @Test
    public void shouldReturnAllFlights() throws ParseException {
       //Mockito.when(dao.findAllFlights()).thenReturn();
        Assert.assertEquals(120, dao.findAllFlights().size());
    }

    @Test
    public void shouldFindFlightsWithOriginDestination() {
      Mockito.when(dao.findFlightsWithOriginDestination("Hyd", "Blr", departureDate))
               .thenReturn(test.getFlightsWithSourceDestination());
        Assert.assertEquals(3, dao.findFlightsWithOriginDestination("Hyd", "Blr", departureDate).size());
    }

    @Test
    public void shouldFindFlightsWithTravelClassBussiness() {
      // Mockito.when(dao.findFlightsWithTravelClass("Hyd", "Blr", departureDate, TravelClass.BUSINESS,5)).thenReturn();
       Assert.assertEquals(3, dao.findFlightsWithTravelClass("Hyd", "Blr", departureDate, TravelClass.BUSINESS,5).size());
    }

    @Test
    public void shouldFindFlightsWithTravelClassFirst() {
        //Mockito.when(dao.findFlightsWithTravelClass("Hyd", "Blr", cal1, TravelClass.FIRST)).thenReturn());
        List<Trip> list= dao.findFlightsWithTravelClass("Hyd", "Blr", departureDate, TravelClass.FIRST,2);
       // list.forEach(flight-> System.out.println("****"+ flight.getRoute().getCalculatedPrice()));
        Assert.assertEquals(3, dao.findFlightsWithTravelClass("Hyd", "Blr", departureDate, TravelClass.FIRST,1).size());
    }
    @Test
    public void shouldFindFlightsWithTravelClassEconomy() {
        //Mockito.when(dao.findFlightsWithTravelClass("Hyd", "Blr", cal1, TravelClass.ECONOMY)).thenReturn());
        List<Trip> list= dao.findFlightsWithTravelClass("Hyd", "Blr", departureDate, TravelClass.ECONOMY,2);
       // list.forEach(flight-> System.out.println("****"+ flight.getRoute().getCalculatedPrice()));
        Assert.assertEquals(3, dao.findFlightsWithTravelClass("Hyd", "Blr", departureDate, TravelClass.ECONOMY,1).size());
    }



    public List<Route> fillFlightDetails() throws ParseException {
       listOfFlights = dao.setFlights();
       return listOfFlights;
    }
    public List<Trip> getFlightsWithSourceDestination() {
        List<Trip> availableFlights;
        availableFlights = dao.findFlightsWithOriginDestination("Hyd", "Blr", departureDate);

        return availableFlights;
    }
}